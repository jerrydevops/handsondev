gcloud compute instances create db01 --image-project=debian-cloud --image-family=debian-10 --metadata-from-file=startup-script=sql.sh --zone=us-central1-a --network=default --machine-type=e2-micro --tags=sql
sleep 30
gcloud compute instances create mc01 --image-project=debian-cloud --image-family=debian-10 --metadata-from-file=startup-script=memcache.sh --zone=us-central1-a --network=default --machine-type=e2-micro --tags=memcache
sleep 20
gcloud compute instances create rmq01 --image-project=debian-cloud --image-family=debian-10 --metadata-from-file=startup-script=rabbit.sh --zone=us-central1-a --network=default --machine-type=e2-micro --tags=rabbitmq
sleep 20
gcloud compute instances create web01 --image-project=debian-cloud --image-family=debian-10 --metadata-from-file=startup-script=nginx.sh --zone=us-central1-a --network=default --machine-type=e2-micro --tags=nginx
sleep 20
gcloud compute instances create app01 --image-project=debian-cloud --image-family=debian-10 --metadata-from-file=startup-script=tomcat.sh --zone=us-central1-a --network=default --machine-type=e2-micro --tags=tomcat
#1046397937390@cloudservices.gserviceaccount.com
